﻿using AgileMQ.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderService.Directories.Ecommerce.Shipping.Models
{
	[QueuesConfig(Directory = "Ecommerce", Subdirectory = "Shipping", ResponseEnabled = true)]
	public class PackageGroup
	{
		public long? Id { get; set; }

		[Required]
		public long? OrderId { get; set; }
	}
}
