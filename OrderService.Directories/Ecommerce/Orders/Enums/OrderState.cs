﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.Directories.Ecommerce.Orders.Enums
{
	public enum OrderState
	{
		New = 10,
		Confirmed = 20,
		Processing = 30,
		Shipped = 40
	}
}
