﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using Microsoft.EntityFrameworkCore;
using OrderService.Data;
using OrderService.Data.Enums;
using OrderService.Data.Models;
using OrderService.Loggers;
using OrderService.Subscribers;
using OrderService.Utilities;
using System;
using System.Linq;

namespace OrderService
{
	public class Program
	{
		public static void Main(string[] args)
		{
			using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=3;PrefetchCount=100;AppId=OrderService"))
			{
				bus.Logger = new ConsoleLogger();

				bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

				bus.Suscribe(new OrderHeaderSubscriber());
				bus.Suscribe(new OrderRowSubscriber());

				Console.WriteLine("OrderService Ready!");
				Console.ReadLine();
			}
		}
	}
}