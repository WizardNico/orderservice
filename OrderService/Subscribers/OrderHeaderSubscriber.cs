﻿using AgileMQ.DTO;
using AgileMQ.Interfaces;
using OrderService.Data;
using OrderService.Directories.Ecommerce.Orders.Models;
using OrderService.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OrderService.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using OrderService.Data.Exceptions;
using AgileMQ.Containers;
using AgileMQ.Extensions;
using DataModels = OrderService.Data.Models;
using OrderService.Data.Enums;
using OrderService.Directories.Ecommerce.Products.Models;
using OrderService.Directories.Ecommerce.Shipping.Models;

namespace OrderService.Subscribers
{
	public class OrderHeaderSubscriber : IAgileSubscriber<OrderHeader>
	{
		public IAgileBus Bus { get; set; }

		public async Task<OrderHeader> GetAsync(OrderHeader model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.Order order = await ctx.Orders.FindByIdAsync(model.Id.Value);

			if (order == null)
				throw new ObjectNotFoundException("Order not found, a wrong ID was used.");

			model = Mapper.Map(order);

			return (model);
		}

		public async Task<OrderHeader> PostAsync(OrderHeader model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.Order order = new DataModels.Order()
			{
				CreationDate = DateTime.Now,
				Note = model.Note,
				State = OrderState.New,
				ShippingAddressId = model.ShippingAddressId.Value,
				UpdateDate = DateTime.Now,
				UserId = model.UserId.Value,
				OrderRows = new List<DataModels.OrderRow>()
			};

			await ctx.Orders.AddAsync(order);
			await ctx.SaveChangesAsync();

			model = Mapper.Map(order);

			return (model);
		}

		public async Task PutAsync(OrderHeader model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.Order order = await ctx.Orders.FindByIdAsync(model.Id.Value);

			if (order == null)
				throw new ObjectNotFoundException("Order not found, a wrong ID was used.");

			if (order.State != OrderState.New)
				throw new InvalidOperationException("Order is confirmed, you can not modify this order now.");

			order.Note = model.Note;
			order.State = (OrderState)model.State;
			order.ShippingAddressId = model.ShippingAddressId.Value;
			order.UpdateDate = DateTime.Now;
			order.UserId = model.UserId.Value;

			await ctx.SaveChangesAsync();

			model = Mapper.Map(order);

			//Se l'ordine viene confermato notifico l'evento
			if (order.State == OrderState.Confirmed)
				await Bus.NotifyAsync(model, "Confirmed");
		}

		public async Task DeleteAsync(OrderHeader model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.Order order = await ctx.Orders.FindByIdAsync(model.Id.Value);

			if (order == null)
				throw new ObjectNotFoundException("Order not found, a wrong ID was used.");

			if (order.State != OrderState.New)
				throw new InvalidOperationException("Order is confirmed, you can not modify this order now.");

			//Elimino prima tutte le righe dell'ordine
			foreach (DataModels.OrderRow orr in order.OrderRows)
			{
				ctx.OrderRows.Remove(orr);
			}

			//Poi elimino la testata
			ctx.Orders.Remove(order);
			await ctx.SaveChangesAsync();
		}

		public async Task<Page<OrderHeader>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			double? totalPrice = filter.Get<double>("totalPrice");
			OrderState? state = filter.Get<OrderState>("state");

			Page<OrderHeader> page = new Page<OrderHeader>()
			{
				Items = new List<OrderHeader>()
			};
			List<DataModels.Order> orderList = await ctx.Orders.GetListAsync(state, totalPrice, pageSize, pageIndex);

			foreach (DataModels.Order order in orderList)
			{
				page.Items.Add(Mapper.Map(order));
			}

			page.TotalItemCount = await ctx.Orders.CountAsync(state, totalPrice);
			page.PageCount = (int)page.TotalItemCount / pageSize;

			return (page);
		}

		public Task<List<OrderHeader>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public async Task ConfirmedAsync(OrderHeader model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo l'ordine relativo
			DataModels.Order order = await ctx.Orders.FindByIdAsync(model.Id.Value);

			//Per ogni riga dell'ordine vado a scalare la quantità relativa
			foreach (DataModels.OrderRow row in order.OrderRows)
			{
				Product product = new Product()
				{
					Code = row.ProductCode,
					Movements = row.Quantity
				};
				await Bus.NotifyAsync(product, "Purchased");
			}

			//Creo la lista dei prodotti relativi all'ordine in esame
			List<Product> productList = new List<Product>();
			foreach (DataModels.OrderRow row in order.OrderRows)
			{
				Product product = new Product()
				{
					Code = row.ProductCode
				};
				product = await Bus.GetAsync(product);
				productList.Add(product);
			}

			//Raggruppo per categoria
			List<IGrouping<string, Product>> grps = productList
				.GroupBy(pro => pro.CategoryCode)
				.ToList();

			//Vado ad inserire il nuovo package-group relativo a questo ordine
			PackageGroup group = new PackageGroup
			{
				OrderId = order.Id,
			};
			group = await Bus.PostAsync(group);

			foreach (IGrouping<string, Product> grp in grps)
			{
				//Prendo il codice dei prodotti
				List<string> productCodes = grp
					.Select(pro => pro.Code)
					.ToList();

				//Prendo gli id degli order-row relativi ai prodotti della categoria in esame
				List<long> orderRowsId = order.OrderRows
					.Where(orr => productCodes.Contains(orr.ProductCode) == true)
					.Select(orr => orr.Id)
					.ToList();

				//Creo il messaggio package con gli id degli order-row contenenti prodotti della stessa categoria nello stesso ordine
				Package package = new Package
				{
					GroupId = group.Id,
					OrderRowsId = orderRowsId,
				};

				//Notifico l'evento
				await Bus.NotifyAsync(package, "Created");
			}
		}

		public async Task CompleteAsync(OrderHeader model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo l'ordine relativo
			DataModels.Order order = await ctx.Orders.FindByIdAsync(model.Id.Value);

			//Aggiorno lo stato
			order.State = OrderState.Processing;

			//Salvo il cambiamento
			await ctx.SaveChangesAsync();
		}
	}
}
