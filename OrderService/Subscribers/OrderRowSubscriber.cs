﻿using AgileMQ.Interfaces;
using OrderService.Directories.Ecommerce.Orders.Models;
using System;
using System.Collections.Generic;
using System.Text;
using AgileMQ.Containers;
using AgileMQ.DTO;
using System.Threading.Tasks;
using OrderService.Data;
using DataModels = OrderService.Data.Models;
using OrderService.Data.Repositories;
using OrderService.Data.Exceptions;
using OrderService.Utilities;
using OrderService.Directories.Ecommerce.Products.Models;
using System.Linq;
using AgileMQ.Extensions;
using OrderService.Data.Enums;

namespace OrderService.Subscribers
{
	public class OrderRowSubscriber : IAgileSubscriber<OrderRow>
	{
		public IAgileBus Bus { get; set; }

		public async Task<OrderRow> GetAsync(OrderRow model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo la riga
			DataModels.OrderRow row = await ctx.OrderRows.FindByIdAsync(model.Id.Value);

			if (row == null)
				throw new ObjectNotFoundException("OrderRow not found, a wrong ID was used.");

			//Prendo l'ordine relativo a questa riga
			DataModels.Order order = await ctx.Orders.FindByIdAsync(row.OrderId);

			if (order == null)
				throw new ObjectNotFoundException("Order not found, a wrong ID was used.");

			//Aggancio il riferimento dell'ordine alla riga
			row.Order = order;

			model = Mapper.Map(row);

			return (model);
		}

		public async Task<OrderRow> PostAsync(OrderRow model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo l'ordine relativo a questa riga
			DataModels.Order order = await ctx.Orders.FindByIdAsync(model.OrderId.Value);

			if (order == null)
				throw new ObjectNotFoundException("Order not found, a wrong ID was used.");

			if (order.State != OrderState.New)
				throw new InvalidOperationException("Order is confirmed, you can not modify this order now.");

			//Prendo il prodotto relativo alla riga
			Product product = new Product()
			{
				Code = model.ProductCode
			};
			product = await Bus.GetAsync(product);

			//Creo un nuovo record
			DataModels.OrderRow newRow = new DataModels.OrderRow()
			{
				CreationDate = DateTime.Now,
				UpdateDate = DateTime.Now,
				Order = order,
				OrderId = order.Id,
				ProductCode = product.Code,
				Price = GetPrice(product),
				Quantity = model.Quantity.Value,
				OrderRowOptions = new List<DataModels.OrderRowOption>()
			};
			//Aggiungo le eventuali opzioni
			if (model.Options != null)
				model.Options.ForEach(opt => newRow.OrderRowOptions.Add(new DataModels.OrderRowOption() { Code = opt, OrderRow = newRow }));

			await ctx.OrderRows.AddAsync(newRow);

			model = Mapper.Map(newRow);
			model.Validate();
			await ctx.SaveChangesAsync();

			model.Id = newRow.Id;
			return (model);
		}

		public async Task PutAsync(OrderRow model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo la riga specificata per la modifica
			DataModels.OrderRow row = await ctx.OrderRows.FindByIdAsync(model.Id.Value);

			if (row == null)
				throw new ObjectNotFoundException("OrderRow not found, a wrong ID was used.");

			//Prendo l'ordine relativo a questa riga
			DataModels.Order order = await ctx.Orders.FindByIdAsync(row.OrderId);

			if (order == null)
				throw new ObjectNotFoundException("Order not found, a wrong ID was used.");

			if (order.State != OrderState.New)
				throw new InvalidOperationException("Order is confirmed, you can not modify this row now.");

			//Altrimenti modifico la riga corrente
			Product product = new Product()
			{
				Code = model.ProductCode
			};
			product = await Bus.GetAsync(product);

			row.ProductCode = product.Code;
			row.Price = GetPrice(product);
			row.UpdateDate = DateTime.Now;
			row.Quantity += model.Quantity.Value;
			row.OrderRowOptions = new List<DataModels.OrderRowOption>();

			//Modifico la lista di opzioni
			if (model.Options != null)
				model.Options.ForEach(opt => row.OrderRowOptions.Add(new DataModels.OrderRowOption() { Code = opt, OrderRow = row }));

			//Se la quantità è diventata nulla o negativa cavo la riga
			if (row.Quantity <= 0)
				ctx.OrderRows.Remove(row);

			await ctx.SaveChangesAsync();
		}

		public async Task DeleteAsync(OrderRow model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.OrderRow row = await ctx.OrderRows.FindByIdAsync(model.Id.Value);

			if (row == null)
				throw new ObjectNotFoundException("OrderRow not found, a wrong ID was used.");

			if (row.Order.State != OrderState.New)
				throw new InvalidOperationException("Order is confirmed, you can not modify this row now.");

			ctx.OrderRows.Remove(row);
			await ctx.SaveChangesAsync();
		}

		public Task<Page<OrderRow>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public async Task<List<OrderRow>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			long? orderId = filter.Get<long>("orderId");

			List<OrderRow> items = new List<OrderRow>();

			List<DataModels.OrderRow> rowsList = await ctx.OrderRows.GetListAsync(orderId);

			foreach (DataModels.OrderRow row in rowsList)
			{
				items.Add(Mapper.Map(row));
			}

			return (items);
		}

		private double GetPrice(Product product)
		{
			double totalGrossPrice = product.Options.Sum(opt => opt.AdditionalPrice.Value) + product.Price.Value;
			double tax = ((totalGrossPrice * product.Vat.Value) / 100);

			return totalGrossPrice + tax;
		}
	}
}
