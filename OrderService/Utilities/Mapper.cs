﻿using System.Linq;
using OrderService.Directories.Ecommerce.Orders.Enums;
using DataModels = OrderService.Data.Models;
using EcommerceOrdersModels = OrderService.Directories.Ecommerce.Orders.Models;
using EcommerceOrdersEnums = OrderService.Directories.Ecommerce.Orders.Enums;
using System.Collections.Generic;

namespace OrderService.Utilities
{
	public class Mapper
	{
		public static EcommerceOrdersModels.OrderHeader Map(DataModels.Order order)
		{
			return new EcommerceOrdersModels.OrderHeader()
			{
				Id = order.Id,
				CreationDate = order.CreationDate,
				UpdateDate = order.UpdateDate,
				Note = order.Note,
				ItemsCount = order.OrderRows.Sum(orr => orr.Quantity),
				ShippingAddressId = order.ShippingAddressId,
				State = (OrderState)order.State,
				UserId = order.UserId,
				TotalPrice = order.OrderRows.Sum(orr => orr.Price * orr.Quantity),
			};
		}

		public static EcommerceOrdersModels.OrderRow Map(DataModels.OrderRow orderRow)
		{
			List<string> list = new List<string>();
			if (orderRow.OrderRowOptions != null)
				orderRow.OrderRowOptions.ForEach(orrOpt => list.Add(orrOpt.Code.ToString()));

			return new EcommerceOrdersModels.OrderRow()
			{
				Id = orderRow.Id,
				CreationDate = orderRow.CreationDate,
				Price = orderRow.Price,
				Quantity = orderRow.Quantity,
				UpdateDate = orderRow.UpdateDate,
				OrderId = orderRow.OrderId,
				ProductCode = orderRow.ProductCode,
				Options = list
			};
		}
	}
}
