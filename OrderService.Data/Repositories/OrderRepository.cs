﻿using Microsoft.EntityFrameworkCore;
using OrderService.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using OrderService.Data.Enums;

namespace OrderService.Data.Repositories
{
	public static class OrderRepository
	{
		public static async Task<Order> FindByIdAsync(this DbSet<Order> repository, long id)
		{
			Order result = await repository
				.Include(ord => ord.OrderRows)
				.Where(ord => ord.Id == id)
				.SingleOrDefaultAsync();

			return (result);
		}

		public static async Task<OrderRow> FindByIdAsync(this DbSet<OrderRow> repository, long id)
		{
			OrderRow result = await repository
				.Include(orr => orr.OrderRowOptions)
				.Where(ord => ord.Id == id)
				.SingleOrDefaultAsync();

			return (result);
		}

		public static async Task<OrderRowOption> FindByCodeAsync(this DbSet<OrderRowOption> repository, string code)
		{
			OrderRowOption result = await repository
				.Where(orrOpt => orrOpt.Code.ToLower() == code.ToLower())
				.SingleOrDefaultAsync();

			return (result);
		}

		public static async Task<List<OrderRow>> GetListAsync(this DbSet<OrderRow> repository, long? orderId)
		{
			IQueryable<OrderRow> query = repository
				.Include(ord => ord.OrderRowOptions);

			if (orderId != null)
			{
				query = query.Where(orr => orr.OrderId == orderId);
			}

			List<OrderRow> result = await query
				.ToListAsync();

			return (result);
		}

		public static async Task<List<Order>> GetListAsync(this DbSet<Order> repository, OrderState? state, double? totalPrice, int pageSize, long pageIndex)
		{
			IQueryable<Order> query = repository
				.Include(ord => ord.OrderRows);

			if (totalPrice != null)
			{
				query = query.Where(ord => ord.OrderRows.Sum(orr => orr.Price * orr.Quantity) >= totalPrice);
			}

			if (state != null)
			{
				query = query.Where(ord => ord.State == state);
			}

			List<Order> result = await query
				.Skip(pageSize * ((int)pageIndex - 1))
				.Take(pageSize).ToListAsync();

			return (result);
		}

		public static async Task<int> CountAsync(this DbSet<Order> repository, OrderState? state, double? totalPrice)
		{
			IQueryable<Order> query = repository
				.Include(ord => ord.OrderRows);

			if (totalPrice != null)
			{
				query = query.Where(ord => ord.OrderRows.Sum(orr => orr.Price * orr.Quantity) >= totalPrice);
			}

			if (state != null)
			{
				query = query.Where(ord => ord.State == state);
			}

			//esecuzione della query di conteggio
			int result = await query
				.CountAsync();

			return (result);
		}
	}
}
