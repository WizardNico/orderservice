﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.Data.Exceptions
{
	public class ObjectNotFoundException : Exception
	{
		public ObjectNotFoundException(string message): base(message){}
	}
}
