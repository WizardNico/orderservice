﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.Data.Exceptions
{
	class InvalidOperationException : Exception
	{
		public InvalidOperationException(string message) : base(message) { }
	}
}
