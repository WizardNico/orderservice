﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderService.Data.Models
{
	public class OrderRowOption
	{
		public long Id { get; set; }



		[Required]
		public string Code { get; set; }



		public long OrderRowId { get; set; }
		public OrderRow OrderRow { get; set; }
	}
}
