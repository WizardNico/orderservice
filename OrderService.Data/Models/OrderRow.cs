﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderService.Data.Models
{
	public class OrderRow
	{
		public long Id { get; set; }



		[Required]
		public string ProductCode { get; set; }

		public int Quantity { get; set; }

		public double Price { get; set; }

		public DateTime CreationDate { get; set; }

		public DateTime? UpdateDate { get; set; }



		public long OrderId { get; set; }
		public Order Order { get; set; }



		public List<OrderRowOption> OrderRowOptions { get; set; }
	}
}
