﻿using OrderService.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderService.Data.Models
{
	public class Order
	{
		public long Id { get; set; }



		public OrderState State { get; set; }

		public int UserId { get; set; }

		public DateTime CreationDate { get; set; }

		public DateTime UpdateDate { get; set; }

		public int ShippingAddressId { get; set; }

		[MaxLength(200)]
		public string Note { get; set; }



		public List<OrderRow> OrderRows { get; set; }
	}
}
