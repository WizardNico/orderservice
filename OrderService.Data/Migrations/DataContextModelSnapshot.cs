﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using OrderService.Data;
using OrderService.Data.Enums;

namespace OrderService.Data.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("OrderService.Data.Models.Order", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreationDate");

                    b.Property<string>("Note")
                        .HasMaxLength(200);

                    b.Property<int>("ShippingAddressId");

                    b.Property<int>("State");

                    b.Property<DateTime>("UpdateDate");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("OrderService.Data.Models.OrderRow", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreationDate");

                    b.Property<long>("OrderId");

                    b.Property<double>("Price");

                    b.Property<string>("ProductCode")
                        .IsRequired();

                    b.Property<int>("Quantity");

                    b.Property<DateTime>("UpdateDate");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.ToTable("OrderRows");
                });

            modelBuilder.Entity("OrderService.Data.Models.OrderRowOption", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired();

                    b.Property<long>("OrderRowId");

                    b.HasKey("Id");

                    b.HasIndex("OrderRowId");

                    b.ToTable("OrderRowOptions");
                });

            modelBuilder.Entity("OrderService.Data.Models.OrderRow", b =>
                {
                    b.HasOne("OrderService.Data.Models.Order", "Order")
                        .WithMany("OrderRows")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OrderService.Data.Models.OrderRowOption", b =>
                {
                    b.HasOne("OrderService.Data.Models.OrderRow", "OrderRow")
                        .WithMany("OrderOptions")
                        .HasForeignKey("OrderRowId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
