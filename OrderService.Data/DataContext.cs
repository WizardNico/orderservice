﻿using Microsoft.EntityFrameworkCore;
using OrderService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.Data
{
	public class DataContext : DbContext
	{
		public DbSet<Order> Orders { get; set; }
		public DbSet<OrderRowOption> OrderRowOptions { get; set; }
		public DbSet<OrderRow> OrderRows { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=OrderService;Persist Security Info=True;User ID=sa;Password=080809xx;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
		}
	}
}
